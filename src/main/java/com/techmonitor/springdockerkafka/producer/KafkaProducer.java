package com.techmonitor.springdockerkafka.producer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import com.techmonitor.springdockerkafka.models.User;

@Component
public class KafkaProducer {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaProducer.class);
    
    private final KafkaTemplate<String,Object> kafkaTemplate;
    
    public KafkaProducer(KafkaTemplate<String, Object> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public void sendMessage(User user) {
        LOGGER.info("Producing message {}", user.toString());
        kafkaTemplate.send("my-topic", user);
    }

}
