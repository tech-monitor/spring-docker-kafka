package com.techmonitor.springdockerkafka.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.techmonitor.springdockerkafka.models.User;

@Component
public class KafkaConsumer {
    
    private static final Logger logger = LoggerFactory.getLogger(KafkaConsumer.class);
    
    @KafkaListener(groupId = "groupId", topics = "my-topic", containerFactory = "kafkaListenerContainerFactory")
    public void consume(User user, Acknowledgment ack) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String jsonUser = mapper.writeValueAsString(user);
        logger.info("----------------> Message consumed: {} ",jsonUser);
        ack.acknowledge();
    }

    
}
