package com.techmonitor.springdockerkafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringDockerKafkaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringDockerKafkaApplication.class, args);
	}

}
