package com.techmonitor.springdockerkafka;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.techmonitor.springdockerkafka.models.User;
import com.techmonitor.springdockerkafka.producer.KafkaProducer;

@Component
public class KafkaRunner implements CommandLineRunner {
    
    private final KafkaProducer producer;
    
    public KafkaRunner(KafkaProducer producer) {
        this.producer = producer;
    }

    @Override
    public void run(String... args) throws Exception {
       User user1 = new User("James","Bond");
       User user2 = new User("Jason","Bourne");
       User user3 = new User("Jack","Ryan");
       
       List<User> users = new ArrayList<>();
       users.add(user1);
       users.add(user2);
       users.add(user3);
       
       users.stream().forEach(user -> {
           producer.sendMessage(user);
           }
       );
       
        
    }

}
